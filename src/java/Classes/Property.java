/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Classes;

/**
 *
 * @author Stuart
 */
public class Property {
    protected int propertyID;
    protected int houseNo;
    protected String postcode;
    protected int tenantID;
    protected int landlordID;
    protected int noOfBeds;
    protected String propertyType;
    protected String propertyDescription;
    protected int marketTypeID;
    protected String addressLine1;
    protected String addressLine2;
    protected String county;
    protected String town_City;

    public Property(int propertyID, int houseNo, String postcode, int tenantID, int landlordID, int noOfBeds, String propertyType, String propertyDescription, int marketTypeID, String addressLine1, String addressLine2, String county, String town_City) {
        this.propertyID = propertyID;
        this.houseNo = houseNo;
        this.postcode = postcode;
        this.tenantID = tenantID;
        this.landlordID = landlordID;
        this.noOfBeds = noOfBeds;
        this.propertyType = propertyType;
        this.propertyDescription = propertyDescription;
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.county = county;
        this.town_City = town_City;
        this.marketTypeID = marketTypeID;
    }

    public int getPropertyID() {
        return propertyID;
    }

    public void setPropertyID(int propertyID) {
        this.propertyID = propertyID;
    }

    public int getHouseNo() {
        return houseNo;
    }

    public void setHouseNo(int houseNo) {
        this.houseNo = houseNo;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public int getTenantID() {
        return tenantID;
    }

    public void setTenantID(int tenantID) {
        this.tenantID = tenantID;
    }

    public int getLandlordID() {
        return landlordID;
    }

    public void setLandlordID(int landlordID) {
        this.landlordID = landlordID;
    }

    public int getNoOfBeds() {
        return noOfBeds;
    }

    public void setNoOfBeds(int noOfBeds) {
        this.noOfBeds = noOfBeds;
    }

    public String getPropertyType() {
        return propertyType;
    }

    public void setPropertyType(String propertyType) {
        this.propertyType = propertyType;
    }

    public String getPropertyDescription() {
        return propertyDescription;
    }

    public void setPropertyDescription(String propertyDescription) {
        this.propertyDescription = propertyDescription;
    }

    public int getMarketTypeID() {
        return marketTypeID;
    }

    public void setMarketTypeID(int marketTypeID) {
        this.marketTypeID = marketTypeID;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getTown_City() {
        return town_City;
    }

    public void setTown_City(String town_City) {
        this.town_City = town_City;
    }
    
    
}
