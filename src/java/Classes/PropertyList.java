/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Classes;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Stuart
 */
public class PropertyList {
    private ArrayList<Property> list;
    
    public PropertyList() {
        list = new ArrayList<Property>();
    }
    
    public PropertyList(ResultSet rs) {
        this();
        try {
            while (rs.next()) {
                list.add(new Property(rs.getInt(1),
                                      rs.getInt(2),
                                      rs.getString(3),
                                      rs.getInt(4),
                                      rs.getInt(5),
                                      rs.getInt(6),
                                      rs.getString(7),
                                      rs.getString(8),
                                      rs.getInt(9),
                                      rs.getString(10),
                                      rs.getString(11),
                                      rs.getString(12),
                                      rs.getString(13)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    
    public void addProperty(Property newProperty) {
        list.add(newProperty);
    }
    
    public Property retrievePropertyAt(int index) {
        if (index < 0 || index >= list.size())
            return null;
        else
            return list.get(index);
    }
    
    public int size() {
        return list.size();
    }
    
    public Property getPropertyAt(int index) {
        if (index < 0 || index >= list.size())
            return null;
        else
            return list.get(index);
    }
}
