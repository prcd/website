<%-- 
    Document   : index
    Created on : Apr 3, 2014, 8:43:40 PM
    Author     : adamgray
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/main.css"/>
        <link rel="stylesheet" type="text/css" href="css/blueberry.css" />
        <title>JSP Page</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
            <script src="js/jquery.blueberry.js"></script>

            <script>
            $(window).load(function() {
                    $('.blueberry').blueberry();
            });
        </script>
    </head>
    <body>
        <jsp:include page="includes/nav.jsp"/>
        <jsp:include page="includes/header.jsp" />
        <div id="contentContainer">
            <br />
            <!-- slider -->
            <div class="blueberry">
              <ul class="slides">
                <li><img src="images/page/IndexProperty1.jpg" /></li>
                <li><img src="images/page/IndexProperty2.jpg" /></li>
                <li><img src="images/page/IndexProperty3.jpg" /></li>
              </ul>
            </div>
            <!-- slider -->
            <div id="content">
                <div class="clear-container">
                    <h1>About</h1>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur posuere commodo purus
                    sed congue. Nulla blandit sodales viverra. Fusce ac porta felis, tristique gravida ante. Aliquam
                    erat volutpat. Vestibulum semper aliquam tempor. Vestibulum arcu magna, ultrices nec lobortis sit
                    amet, commodo pellentesque nisi. Suspendisse consequat sapien sed accumsan placerat. Proin consequat
                    sodales lorem, at convallis massa malesuada eget. Donec ullamcorper, lectus ut aliquet suscipit, felis
                    erat elementum massa, sed imperdiet neque ante ac tellus. Donec dignissim eros enim, non pretium risus
                    vestibulum ac.
                </div>
                <div class="green-container">
                    
                </div>
                <div class="gold-container">
                    
                </div>
                <div class="black-container">
                    
                </div>
                <span class="dark-green-bold" style="margin-left: 50px;">PROPERTIES FOR <span class="dark-yellow-bold">SALE</span> IN</span>
                <span class="dark-green-bold" style="margin-left: 300px;">PROPERTIES TO <span class="dark-yellow-bold">RENT</span> IN</span><br/><br/>
                <span style="margin-left: 50px;"><a href="#">Devonport</a>  <a href="#">Mannamead</a>  <a href="#">Plymstock</a>  <a href="#">Stonehouse</a></span>
                <span style="margin-left: 260px; margin-top: -50px;"><a href="#">Devonport</a>  <a href="#">Mannamead</a>  <a href="#">Plymstock</a>  <a href="#">Stonehouse</a></span>
               <br/>
               <br/>
            </div>
        </div>
        <jsp:include page="includes/footer.jsp" />
    </body>
</html>
