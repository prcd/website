<%-- 
    Document   : header
    Created on : Apr 3, 2014, 8:21:00 PM
    Author     : adamgray
--%>
<div id="header">
    <div id="header-left">
        <span class="container-headings">PRDCD</span>
        <span class="container-headings" style="color: #27A6BA;">PROJECT</span>
        <br/>
        <span class="normal-light-grey">Estate Agent Website</span>
    </div>
    <div id="header-middle">
        <form name="search" action="results.jsp" method="get">
            <label for="squery" class="normal-light-green" style="margin-left: -480px;">Property Search</label><br/>
            <div class="search-bar-box"><input class="search-bar" type="text" name="s" placeholder="e.g. Plymouth or PL4" /></div>
            <button class="button-green" type="submit" name="buy">To Buy</button>
            <button class="button-green" type="submit" name="rent">To Rent</button>
        </form>
    </div>
</div>