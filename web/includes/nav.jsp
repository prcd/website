<%-- 
    Document   : nav
    Created on : Apr 4, 2014, 10:15:25 PM
    Author     : adamgray
--%>

<div id="nav">
    <ul>
        <a href="index.jsp"><li>Home</li></a>
        <a href="#"><li>Buy</li></a>
        <a href="#"><li>Rent</li></a>
        <a href="#"><li>Blog</li></a>
        <a href="#"><li>Contact</li></a>
    </ul>
    <ul style="display: inline; float: right; margin-top: -28px; ">
        <a href="#"><li>Log In</li></a>
        <a href="#"><li>Create Account</li></a>
    </ul>
</div>