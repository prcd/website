<%-- 
    Document   : footer
    Created on : Apr 3, 2014, 8:21:12 PM
    Author     : adamgray
--%>

<nav id="footer">
    <span style="margin-left: 10px;"><a href="#">Sitemap</a> | <a href="#">Terms of Use</a> | <a href="#">Privacy Policy</a></span>
    <span style="display: inline; float: right; margin-right: 10px;">Designed by Group PRDCD for Integrated Project 2014</span>
</nav>